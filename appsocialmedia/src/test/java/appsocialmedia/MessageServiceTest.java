package appsocialmedia;

import java.util.List;

import org.junit.Test;

import com.practice.restws.appsocialmedia.model.Message;
import com.practice.restws.appsocialmedia.service.MessageService;

import junit.framework.Assert;

public class MessageServiceTest {
	

	@Test
	public void testGetMessages()
	{
		MessageService m=new MessageService();
		List<Message> lst=m.getMessages();
		Assert.assertNotNull(lst);
	}
	

	@Test
	public void testGetName()
	{
		MessageService m=new MessageService();
		boolean ans=m.getName("Himanshu");
		Assert.assertEquals(true, ans);
	}	
}
